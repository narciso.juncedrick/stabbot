import os
import youtube_dl
import time
from helper import PicnicException


class VideoNotFoundException(PicnicException):
    pass


# ####################### #
# ## functions ########## #
# ####################### #

def search_and_download_video(submission):
    filename = 'download.mkv'
    ydl = youtube_dl.YoutubeDL({
        "outtmpl": filename,
        "quiet": True,
        "restrictfilenames": True})
    ydl.download([submission.url])

    # wait 5 secodns because sometimes trash file still clutter the folder
    time.sleep(5)
    for f in os.listdir("."):
        filename, file_extension = os.path.splitext(f)
        if file_extension == '.part':
            continue
        return f
    raise VideoNotFoundException("No Video found")
